/** @param {NS} ns **/

// Copyright 2022 Frank Braker All Rights Reserved
// Do not copy any part of this program without written permission of the author
//
// Description: Bitburner (videogame) stock trading program
// 

const CashSafetyMargin = 400000;            // whatever money we have, we'll hold on to this margin for safety
const MinimumInvestmentAllowed = 3000000;   // don't allow tiny investments

var CashToInvest = 0;

var globals;

class Singleton {
    // class    
    #idxMessages;
    Allcash;
    sumMaxDaysPassedAllSymbols;
    lastMessage;
    #messages;
    allSymbols;

    totalFunds() {
        var totalFunds = this.Allcash;
        for (var s of this.allSymbols) {
            var name = s.name;
            totalFunds += this.ns.stock.getPosition(name)[0] * this.ns.stock.getPrice(name);
        }
        return totalFunds;
    }

    updateCashToInvest() {
        this.Allcash = this.totalFunds() - CashSafetyMargin;
    }

    constructor(_Allcash, _ns, _symbolsArray) {

        this.Allcash = _Allcash;
        this.ns = _ns;
        this.allSymbols = _symbolsArray;

        this.#idxMessages = 0;
        this.sumMaxDaysPassedAllSymbols = 0;
        this.lastMessage = "";
        this.#messages = [
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            ".",
            "."
        ];
        this.updateCashToInvest();
    }

    showMessage(_message) {

        this.lastMessage = _message;
        this.#messages[this.#idxMessages] = _message;
        this.#idxMessages = (this.#idxMessages + 1) % this.#messages.length;

        this.ns.print(this.lastMessage);
    }

    showStatus() {
        var totalFunds = this.Allcash;
        for (var s of this.allSymbols) {
            var name = s.name;
            totalFunds += this.ns.stock.getPosition(name)[0] * this.ns.stock.getPrice(name);
        }

        var s = this.allSymbols[0];
        var cashString = (this.Allcash / 1000000000).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        var totalString = (totalFunds / 1000000000).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
        this.ns.print("Allcash = ", cashString, "B, totalFunds=", totalString, "B");

        var i = 0;
        while (i < this.#messages.length) {

            var j = this.#idxMessages;                    // always points to the oldest message so show it first
            j = (j + i) % this.#messages.length;

            this.ns.print(this.#messages[j]);
            i++;
        }
    }
}

// keeps track of last price, slow average, fast average, last forecast, name
class Symbol {
    #indexPriceHistoryFast;
    #indexPriceHistorySlow;
    #FastHistoryAverage;
    #SlowHistoryAverage;
    #priceHistoryFast;
    #priceHistorySlow;
    #forecastTriggered;
    basis;

    constructor(_name, _forecast, _cash, _daysSinceSell, _price, _shares) {
        this.name = _name;
        this.shares = _shares;
        this.forecast = _forecast;
        this.cash = _cash;
        this.daysSinceSell = _daysSinceSell;
        this.price = _price;
        this.#indexPriceHistoryFast = 0;
        this.#indexPriceHistorySlow = 0;
        this.#FastHistoryAverage = 0;
        this.#SlowHistoryAverage = 0;
        this.#forecastTriggered = false;
        this.#priceHistoryFast = [
            _price,
            _price,
            _price,
            _price,
            _price];
        this.#priceHistorySlow = [
            _price,
            _price,
            _price,
            _price,
            _price,
            _price,
            _price,
            _price,
            _price,
            _price,
            _price];
        this.basis = 0;
    }

    setPrice(ns, _price) {
        if (_price != this.price) {     // assume the price always changes from day to day
            //ns.print("the price changed for ", this.name, " to ", _price, " daysSinceSell= ", this.daysSinceSell);
            this.price = _price;
            this.daysSinceSell++;

            // calculate fast average
            this.#indexPriceHistoryFast = (this.#indexPriceHistoryFast + 1) % this.#priceHistoryFast.length;
            this.#priceHistoryFast[this.#indexPriceHistoryFast] = _price;

            var x = 0;
            for (var p of this.#priceHistoryFast) {
                x += p;
            }
            this.#FastHistoryAverage = x / this.#priceHistoryFast.length;

            // calculate slow average
            this.#indexPriceHistorySlow = (this.#indexPriceHistorySlow + 1) % this.#priceHistorySlow.length;
            this.#priceHistorySlow[this.#indexPriceHistorySlow] = _price;

            var x = 0;
            for (var p of this.#priceHistorySlow) {
                x += p;
            }
            this.#SlowHistoryAverage = x / this.#priceHistorySlow.length;
        }
    }

    // Figure out if it's time to buy this stock
    timeToBuy(ns, _forecast) {
        var forecastChanged = ((_forecast > 0.5) && (this.forecast < 0.5))
            || ((_forecast < 0.5) && (this.forecast > 0.5));
        var retvar = false;

        if (_forecast < 0.5) {
            //ns.print("forecastTriggered = false _forecast=",_forecast);
            this.#forecastTriggered = false;
        } else if ((_forecast > 0.5) && (this.forecast < 0.5) || this.#forecastTriggered) {
            this.#forecastTriggered = true;
            //globals.lastMessage = "got trigger for " + this.name + " _forecast=" + _forecast + ":" + this.forecast;
            //ns.print(globals.lastMessage);
            if (this.#FastHistoryAverage > this.#SlowHistoryAverage) {
                //ns.print("!!timeToBuy=true!!");
                retvar = true;
            }
        }

        if (forecastChanged) {
            this.forecast = _forecast;
        }

        return (retvar);
    }
}

export async function updateSymbolData(ns, s) {
    var currentPrice = ns.stock.getPrice(s.name);
    s.setPrice(ns, currentPrice);
}

// reallocate Allcash (more money goes to those who have had the most days pass since they sold)
export async function reallocateCash(ns, symbols) {

    var maxDaysPassedSymbol = new Symbol("FAKESYMBOL", 0, 0, 0, 0, 0);   // FAKESYMBOL is always daysSinceSell=0
    globals.sumMaxDaysPassedAllSymbols = 0;
    for (var s of symbols) {
        if (maxDaysPassedSymbol.daysSinceSell > s.daysSinceSell) {
            maxDaysPassedSymbol.name = s.name;
            maxDaysPassedSymbol.daysSinceSell = s.daysSinceSell;

            //globals.lastMessage = "! maxDaysPassedSymbol = " + s.name + " days = " + s.daysSinceSell;
            //ns.print(globals.lastMessage);
            globals.showMessage("! maxDaysPassedSymbol = " + s.name + " days = " + s.daysSinceSell);
        }
        // update globals.
        globals.sumMaxDaysPassedAllSymbols += s.daysSinceSell;
    }

    if (maxDaysPassedSymbol.daysSinceSell > 0) {        // any candidate?
        // reallocate Allcash in a weighted fashion 
        for (var s of symbols) {
            s.cash = s.daysSinceSell * (globals.Allcash / symbols.length) / globals.sumMaxDaysPassedAllSymbols;
            ns.print("! reallocated ", s.cash, " to ", s.name);
        }
        ns.sleep(750);
    }
}

export async function processSymbol(ns, s) {

    // if we currently own no shares, look hold in a loop until we see a change from - to +
    s.shares = ns.stock.getPosition(s.name)[0];
    var currentForecast = ns.stock.getForecast(s.name);

    if (s.timeToBuy(ns, currentForecast)) {       // time to buy?
        if (s.shares == 0) {
            // we can buy
            var currentPrice = ns.stock.getPrice(s.name);

            // enforce the MinimumInvestmentAllowed, to concentrate investment if needed
            var buyingCash = Math.min(MinimumInvestmentAllowed, globals.Allcash);
            buyingCash = Math.max(s.cash, buyingCash);

            // don't bother if we've already spent most of our cash
            if (buyingCash >= MinimumInvestmentAllowed) {

                var buyshares = parseInt((buyingCash - 200000) / currentPrice) - 2; // lots of rounding errors
                var availableShares = ns.stock.getMaxShares(s.name);
                buyshares = Math.min(buyshares, availableShares);
                ns.stock.buy(s.name, buyshares);
                s.shares = ns.stock.getPosition(s.name)[0];
                var spent = (s.shares * currentPrice) + 100000;
                s.cash -= spent;
                s.basis = spent;
                globals.Allcash -= spent;
                globals.showMessage("Buy  " + ((s.shares / 1000000).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] + "M         ").substring(0, 10) + "of " + (s.name + "    ").substring(0, 5) + " for " + ((spent / 1000000).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] + "M      ").substring(0, 14));// prvCast= " + s.forecast.toString().substring(0, 4) + " forcast= " + currentForecast.toString().substring(0, 4) );
                // source: https://stackoverflow.com/questions/4187146/truncate-number-to-two-decimal-places-without-rounding

                // if we didn't use all our cash, redistribute it
                if (s.cash != 0) {
                    s.cash = 0;
                    reallocateCash(ns, globals.allSymbols);
                }

            }
        } else {
            // if we own any shares at all:
            s.daysSinceSell = 0;                                    // days since buy or sell - because we're frozen
        }
    } else
        if (currentForecast < 0.5) {    // time to sell not needed: && s.forecast > 0.5

            if (s.shares > 0) {
                // we have
                var currentPrice = ns.stock.getPrice(s.name);
                ns.stock.sell(s.name, s.shares);
                s.daysSinceSell = 0;                                    // days since buy or sell - because we're frozen

                var gains = s.shares * currentPrice;
                var relativegains = gains - s.basis;
                s.basis = 0;
                s.cash += gains;
                globals.Allcash += gains;
                globals.showMessage("Sell " + ((s.shares / 1000000).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] + "M         ").substring(0, 10) + "of " + (s.name + "    ").substring(0, 5) + " for " + ((s.shares * currentPrice / 1000000).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] + "M      ").substring(0, 14) + " " + ((relativegains / 1000000).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0] + "M         ").substring(0, 14));// prvcast= " + s.forecast.toString().substring(0, 4) + " forcast= " + currentForecast.toString().substring(0, 4) );
                // source: https://stackoverflow.com/questions/4187146/truncate-number-to-two-decimal-places-without-rounding

                s.shares = 0;
            }
        }
}

export async function main(ns) {

    ns.print("Starting script here");

    CashToInvest = ns.getPlayer().money - CashSafetyMargin;

    // Eventually "CashToInvest" should be computed from an API
    globals = new Singleton(CashToInvest, ns, [
        new Symbol("ECP", ns.stock.getForecast("ECP"), 0, 0, 0, 0),
        new Symbol("MGCP", ns.stock.getForecast("MGCP"), 0, 0, 0, 0),
        new Symbol("BLD", ns.stock.getForecast("BLD"), 0, 0, 0, 0),
        new Symbol("CLRK", ns.stock.getForecast("CLRK"), 0, 0, 0, 0),
        new Symbol("OMTK", ns.stock.getForecast("OMTK"), 0, 0, 0, 0),

        new Symbol("FSIG", ns.stock.getForecast("FSIG"), 0, 0, 0, 0),
        new Symbol("KGI", ns.stock.getForecast("KGI"), 0, 0, 0, 0),
        new Symbol("FLCM", ns.stock.getForecast("FLCM"), 0, 0, 0, 0),
        new Symbol("STM", ns.stock.getForecast("STM"), 0, 0, 0, 0),
        new Symbol("DCOMM", ns.stock.getForecast("DCOMM"), 0, 0, 0, 0),
        new Symbol("HLS", ns.stock.getForecast("HLS"), 0, 0, 0, 0),
        new Symbol("VITA", ns.stock.getForecast("VITA"), 0, 0, 0, 0),
        new Symbol("ICRS", ns.stock.getForecast("ICRS"), 0, 0, 0, 0),
        new Symbol("UNV", ns.stock.getForecast("UNV"), 0, 0, 0, 0),
        new Symbol("AERO", ns.stock.getForecast("AERO"), 0, 0, 0, 0),
        new Symbol("OMN", ns.stock.getForecast("OMN"), 0, 0, 0, 0),
        new Symbol("SLRS", ns.stock.getForecast("SLRS"), 0, 0, 0, 0),
        new Symbol("GPH", ns.stock.getForecast("GPH"), 0, 0, 0, 0),
        new Symbol("NVMD", ns.stock.getForecast("NVMD"), 0, 0, 0, 0),
        new Symbol("WDS", ns.stock.getForecast("WDS"), 0, 0, 0, 0),
        new Symbol("LXO", ns.stock.getForecast("LXO"), 0, 0, 0, 0),
        new Symbol("RHOC", ns.stock.getForecast("RHOC"), 0, 0, 0, 0),
        new Symbol("APHE", ns.stock.getForecast("APHE"), 0, 0, 0, 0),
        new Symbol("SYSC", ns.stock.getForecast("SYSC"), 0, 0, 0, 0),
        new Symbol("CTK", ns.stock.getForecast("CTK"), 0, 0, 0, 0),
        new Symbol("NTLK", ns.stock.getForecast("NTLK"), 0, 0, 0, 0),
        new Symbol("OMGA", ns.stock.getForecast("OMGA"), 0, 0, 0, 0),
        new Symbol("FNS", ns.stock.getForecast("FNS"), 0, 0, 0, 0),
        new Symbol("SGC", ns.stock.getForecast("SGC"), 0, 0, 0, 0),
        new Symbol("JGN", ns.stock.getForecast("JGN"), 0, 0, 0, 0),
        new Symbol("CTYS", ns.stock.getForecast("CTYS"), 0, 0, 0, 0),
        new Symbol("MDYN", ns.stock.getForecast("MDYN"), 0, 0, 0, 0),
        new Symbol("TITN", ns.stock.getForecast("TITN"), 0, 0, 0, 0)
    ]);



    //globals.Allcash = CashToInvest;

    for (var s of globals.allSymbols) {
        s.cash = globals.Allcash / globals.allSymbols.length;
        ns.print("allocated ", s.cash, " to ", s.name);
    }
    await ns.sleep(2000);

    while (true) {                          // yes we never stop...

        // figure out if it's a new day, etc.
        for (var s of globals.allSymbols) {
            await updateSymbolData(ns, s);
        }

        await reallocateCash(ns, globals.allSymbols);

        for (var s of globals.allSymbols) {
            await processSymbol(ns, s);
        }
        await ns.sleep(750);

        //if (globals.lastMessage != null ) {
        //    ns.print("Message= ",globals.lastMessage);
        //}
        globals.showStatus();
    }

    ns.print(ns.args);           //The script arguments must be prefaced with ns as well
}


